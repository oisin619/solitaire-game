A Solitaire application built in Unity3D using C# scripts. The objective is for players to drag and drop cards from 7 piles to 4 piles of each suit in ascending order.
The deck is shuffled randomly before the start of each game and deals out the cards into 7 incrementing piles with the last card being faceup.
Cards in the 7 piles have to be stacked by the player in alternating colours and in descending numerical order.
A "Trip" is available beside the deck that shows the top 3 cards from the deck with only the 1st card in the Trip being able to move to other piles.
Clicking on the deck displays the next 3 cards in the deck as the current Trip.
If the player reaches the end of the deck by cycling through Trips, clicking on the deck again will show the top cards of the deck as the trip and allow the player to cycle through again as many times as they wish.
The player has the ability to reset the game at any time.
When the game is won, a message appears on the screen and asks the player if they would like to play again.

---Retrospective---
The game functions perfectly as a game of solitaire. The only changes I would make would be to add sound effects for certain actions like clicking on cards, and maybe a few graphical tweaks.
Some of the functions used in the scripts could be made more efficient. For a small application such as this, the current functions suffice but there is a little room to improve the logic of certain functions.
The PowerPoint slides attached to the project give a better overview of what I was trying to achieve.